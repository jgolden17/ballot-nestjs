import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { AddContestToBallotCommand } from '../command/add-contest-to-ballot.command';
import { CreateBallotCommand } from '../command/create-ballot.command';
import { DeliverBallotCommand } from '../command/deliver-ballot.command';
import { IContest } from '../domain/contest';
import { GetBallotByIdQuery } from '../query/get-ballot-by-id.query';

@Injectable()
export class BallotService {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  findById(ballotId: ID) {
    return this.queryBus.execute(new GetBallotByIdQuery(ballotId));
  }

  createBallot(title: string) {
    return this.commandBus.execute(new CreateBallotCommand(title));
  }

  deliverBallot(ballotId: ID, to: Email[]) {
    return this.commandBus.execute(new DeliverBallotCommand(ballotId, to));
  }

  addContest(ballotId: ID, contest: Omit<IContest, 'id'>) {
    return this.commandBus.execute(
      new AddContestToBallotCommand(ballotId, contest),
    );
  }
}
