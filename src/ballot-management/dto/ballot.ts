import { BallotStatus } from '../domain/ballot.aggregate';
import { IContest } from '../domain/contest';

export interface IBallotDto {
  id: ID;

  title: string;

  createdAt: Date;

  status: BallotStatus;

  contests: IContest[];
}
