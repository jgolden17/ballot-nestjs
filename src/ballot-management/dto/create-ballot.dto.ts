export interface CreateBallotDto {
  id: ID;
  title: string;
  description?: string;
  createdAt: Date;
}
