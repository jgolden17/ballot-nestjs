export interface CountBallotDto {
  id: ID;
  countedAt: Date;
}
