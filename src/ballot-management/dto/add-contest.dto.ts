import { IContest } from '../domain/contest';

export interface AddContestDto {
  ballotId: ID;
  contest: IContest;
}
