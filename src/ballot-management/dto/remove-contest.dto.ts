export interface RemoveContestDto {
  ballotId: ID;
  contestId: ID;
}
