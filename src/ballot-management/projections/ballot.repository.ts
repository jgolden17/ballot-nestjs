import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { BallotStatus, IBallot } from '../domain/ballot.aggregate';
import { AddContestDto } from '../dto/add-contest.dto';
import { CountBallotDto } from '../dto/count-ballot.dto';
import { CreateBallotDto } from '../dto/create-ballot.dto';
import { RemoveContestDto } from '../dto/remove-contest.dto';
import { BallotDocument } from './ballot.projection';

@Injectable()
export class BallotRepository {
  constructor(
    @InjectModel('ballot')
    private readonly ballotModel: Model<BallotDocument>,
  ) {}

  async findById(id: ID): Promise<IBallot> {
    const result = await this.ballotModel
      .findById(new Types.ObjectId(id))
      .lean();

    return {
      id: result._id.toString(),
      title: result.title,
      createdAt: result.createdAt,
      status: BallotStatus.Ready,
      contests: result.contests.map((contest) => ({
        id: contest._id,
        type: contest.type,
        title: contest.title,
        description: contest.description,
        choices: contest.choices,
      })),
    };
  }

  async create(createBallotDto: CreateBallotDto) {
    const { id, ...rest } = createBallotDto;

    return this.ballotModel.findOneAndUpdate(
      { _id: new Types.ObjectId(id) },
      { $set: rest },
      { upsert: true },
    );
  }

  async count(countBallotDto: CountBallotDto) {
    const { id, countedAt } = countBallotDto;

    return this.ballotModel.findOneAndUpdate(
      { _id: new Types.ObjectId(id) },
      { $set: { countedAt } },
    );
  }

  async addContest(addContestDto: AddContestDto) {
    const { ballotId, contest } = addContestDto;

    return this.ballotModel.findOneAndUpdate(
      {
        _id: ballotId,
      },
      {
        $addToSet: {
          contests: {
            _id: new Types.ObjectId(contest.id),
            ...contest,
          },
        },
      },
    );
  }

  async removeContest(removeContestDto: RemoveContestDto) {
    const { ballotId, contestId } = removeContestDto;

    return this.ballotModel.findOneAndUpdate(
      {
        _id: ballotId,
      },
      {
        $pull: {
          contests: {
            _id: new Types.ObjectId(contestId),
          },
        },
      },
    );
  }
}
