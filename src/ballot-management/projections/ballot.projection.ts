import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ContestType, IContest } from '../domain/contest';

export type BallotDocument = Ballot & Document;

export type ContestDocument = Contest & Document;

export class Contest implements Omit<IContest, 'id'> {
  @Prop()
  title: string;

  @Prop()
  description: string;

  @Prop()
  choices: string[];

  @Prop()
  type: ContestType;
}

@Schema()
export class Ballot {
  @Prop()
  title: string;

  @Prop()
  createdAt: Date;

  @Prop()
  contests: ContestDocument[];
}

export const BallotSchema = SchemaFactory.createForClass(Ballot);
