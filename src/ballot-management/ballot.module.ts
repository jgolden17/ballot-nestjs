import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import { EventSourcingModule } from 'event-sourcing-nestjs';
import { AddContestToBallotHandler } from './command/add-contest-to-ballot.handler';
import { CreateBallotHandler } from './command/create-ballot.handler';
import { DeliverBallotHandler } from './command/deliver-ballot.handler';
import { RemoveContestFromBallotHandler } from './command/remove-contest-from-ballot.handle';
import { RequestDeliverBallotHandler } from './command/request-deliver-ballot.handler';
import { BallotCreatedHandler } from './events/ballot-created.handler';
import { ContestAddedToBallotHandler } from './events/contest-added-to-ballot.handler';
import { ContestRemovedFromBallotHandler } from './events/contest-removed-from-ballot.handler';
import { BallotResolver } from './graphql/resolver';
import { BallotSchema } from './projections/ballot.projection';
import { BallotRepository } from './projections/ballot.repository';
import { GetBallotByIdHandler } from './query/get-ballot-by-id.handler';
import { BallotSagas } from './sagas/ballot.saga';
import { BallotService } from './services/BallotService';

const commandHandlers = [
  CreateBallotHandler,
  AddContestToBallotHandler,
  RemoveContestFromBallotHandler,
  RequestDeliverBallotHandler,
  DeliverBallotHandler,
];

const eventHandlers = [
  BallotCreatedHandler,
  ContestAddedToBallotHandler,
  ContestRemovedFromBallotHandler,
];

const queryHandlers = [GetBallotByIdHandler];

const sagas = [BallotSagas];

@Module({
  imports: [
    CqrsModule,
    EventSourcingModule.forFeature(),
    MongooseModule.forFeature([{ name: 'ballot', schema: BallotSchema }]),
  ],
  providers: [
    BallotResolver,
    BallotService,
    BallotRepository,
    ...commandHandlers,
    ...eventHandlers,
    ...queryHandlers,
    ...sagas,
  ],
})
export class BallotModule {}
