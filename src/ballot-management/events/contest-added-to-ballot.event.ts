import { StorableEvent } from 'event-sourcing-nestjs';
import { EVENT_AGGREGATE } from '../constants';
import { IContest } from '../domain/contest';

export class ContestAddedToBallot extends StorableEvent {
  eventAggregate = EVENT_AGGREGATE;
  eventVersion = 1;

  constructor(public readonly id: ID, public readonly data: IContest) {
    super();
  }
}
