import { IViewUpdater, ViewUpdaterHandler } from 'event-sourcing-nestjs';
import { ContestRemovedFromBallot } from './contest-removed-from-ballot.event';
import { BallotRepository } from '../projections/ballot.repository';

@ViewUpdaterHandler(ContestRemovedFromBallot)
export class ContestRemovedFromBallotHandler
  implements IViewUpdater<ContestRemovedFromBallot>
{
  constructor(private readonly repository: BallotRepository) {}

  async handle(event: ContestRemovedFromBallot) {
    console.log('handling ContestRemovedFromBallot event');

    const { id: ballotId, data } = event;

    this.repository.removeContest({ ballotId, contestId: data.id });
  }
}
