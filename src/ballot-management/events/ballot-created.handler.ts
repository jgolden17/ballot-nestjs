import { IViewUpdater, ViewUpdaterHandler } from 'event-sourcing-nestjs';
import { BallotCreated } from './ballot-created.event';
import { BallotRepository } from '../projections/ballot.repository';

@ViewUpdaterHandler(BallotCreated)
export class BallotCreatedHandler implements IViewUpdater<BallotCreated> {
  constructor(private readonly repository: BallotRepository) {}

  async handle(event: BallotCreated) {
    console.log('handling BallotCreated event');

    this.repository.create({
      id: event.id,
      ...event.data,
    });
  }
}
