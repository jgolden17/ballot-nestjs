import { StorableEvent } from 'event-sourcing-nestjs';
import { EVENT_AGGREGATE } from '../constants';

interface BallotDeliveredData {
  to: Email[];
}

export class BallotDelivered extends StorableEvent {
  eventAggregate = EVENT_AGGREGATE;
  eventVersion = 1;

  constructor(
    public readonly id: ID,
    public readonly data: BallotDeliveredData,
  ) {
    super();
  }
}
