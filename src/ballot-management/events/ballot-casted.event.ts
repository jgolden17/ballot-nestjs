import { StorableEvent } from 'event-sourcing-nestjs';
import { EVENT_AGGREGATE } from '../constants';
import { CreateBallotDto } from '../dto/create-ballot.dto';

export class BallotCasted extends StorableEvent {
  eventAggregate = EVENT_AGGREGATE;
  eventVersion = 1;

  constructor(public readonly id: ID, public readonly data: CreateBallotDto) {
    super();
  }
}
