import { IViewUpdater, ViewUpdaterHandler } from 'event-sourcing-nestjs';
import { ContestAddedToBallot } from './contest-added-to-ballot.event';
import { BallotRepository } from '../projections/ballot.repository';

@ViewUpdaterHandler(ContestAddedToBallot)
export class ContestAddedToBallotHandler
  implements IViewUpdater<ContestAddedToBallot>
{
  constructor(private readonly repository: BallotRepository) {}

  async handle(event: ContestAddedToBallot) {
    console.log('handling ContestAddedToBallot event');

    const { id: ballotId, data: contest } = event;

    this.repository.addContest({ ballotId, contest });
  }
}
