import { IViewUpdater, ViewUpdaterHandler } from 'event-sourcing-nestjs';
import { BallotCounted } from './ballot-counted.event';
import { BallotRepository } from '../projections/ballot.repository';

@ViewUpdaterHandler(BallotCounted)
export class BallotCountedHandler implements IViewUpdater<BallotCounted> {
  constructor(private readonly repository: BallotRepository) {}

  async handle(event: BallotCounted) {
    console.log('handling BallotCounted event');

    const { id, data } = event;

    this.repository.count({ id: id, ...data });
  }
}
