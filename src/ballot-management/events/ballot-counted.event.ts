import { StorableEvent } from 'event-sourcing-nestjs';
import { EVENT_AGGREGATE } from '../constants';
import { CountBallotDto } from '../dto/count-ballot.dto';

export class BallotCounted extends StorableEvent {
  eventAggregate = EVENT_AGGREGATE;
  eventVersion = 1;

  constructor(
    public readonly id: ID,
    public readonly data: OmitId<CountBallotDto>,
  ) {
    super();
  }
}
