import { StorableEvent } from 'event-sourcing-nestjs';
import { EVENT_AGGREGATE } from '../constants';

export interface IDeliverBallotRequestedData {
  to: Email[];
  requestedAt: Date;
}

export class DeliverBallotRequested extends StorableEvent {
  eventAggregate = EVENT_AGGREGATE;
  eventVersion = 1;

  constructor(
    public readonly id: ID,
    public readonly data: IDeliverBallotRequestedData,
  ) {
    super();
  }
}
