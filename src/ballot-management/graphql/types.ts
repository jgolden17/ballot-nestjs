import { createUnionType, Field, InputType, ObjectType } from '@nestjs/graphql';
import { ContestType } from '../domain/contest';

@ObjectType()
export class Contest {
  @Field()
  type: ContestType;

  @Field()
  title?: string;

  @Field()
  description?: string;

  @Field(() => [String])
  choices: string[];
}

@ObjectType({ description: 'Represents a ballot' })
export class Ballot {
  @Field()
  id: ID;

  @Field()
  title: string;

  @Field()
  status: string;

  @Field(() => [Contest])
  contests: Contest[];
}

@InputType()
export class CreateBallotInput {
  @Field()
  title: string;
}

@ObjectType()
export class CreateBallotSuccess {
  @Field()
  message: string;

  @Field(() => Ballot)
  ballot: Ballot;
}

@ObjectType()
export class CreateBallotError {
  @Field()
  message: string;
}

export const CreateBallotResponse = createUnionType({
  name: 'CreateBallotResponse',
  types: () => [CreateBallotSuccess, CreateBallotError],
  resolveType: (response) => {
    if ('ballot' in response) {
      return CreateBallotSuccess;
    }

    return CreateBallotError;
  },
});

@InputType()
export class DeliverBallotInput {
  @Field()
  id: ID;

  @Field(() => [String])
  to: Email[];
}

@ObjectType()
export class DeliverBallotSuccess {
  @Field()
  message: string;

  @Field(() => Ballot)
  ballot: Ballot;
}

@ObjectType()
export class DeliverBallotError {
  @Field()
  message: string;
}

export const DeliverBallotResponse = createUnionType({
  name: 'DeliverBallotResponse',
  types: () => [DeliverBallotSuccess, DeliverBallotError],
  resolveType: (response) => {
    if ('ballot' in response) {
      return DeliverBallotSuccess;
    }

    return DeliverBallotError;
  },
});

@InputType()
export class ContestInput {
  @Field()
  type: ContestType;

  @Field()
  title?: string;

  @Field()
  description?: string;

  @Field(() => [String])
  choices: string[];
}

@InputType()
export class AddContestInput {
  @Field()
  ballotId: ID;

  @Field()
  contest: ContestInput;
}

@ObjectType()
export class AddContestSuccess {
  @Field()
  message: string;

  @Field(() => Ballot)
  ballot: Ballot;
}

@ObjectType()
export class AddContestError {
  @Field()
  message: string;
}

export const AddContestResponse = createUnionType({
  name: 'AddContestResponse',
  types: () => [AddContestSuccess, AddContestError],
  resolveType: (response) => {
    if ('ballot' in response) {
      return AddContestSuccess;
    }

    return AddContestError;
  },
});
