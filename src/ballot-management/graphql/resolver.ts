import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { BallotService } from '../services/BallotService';
import {
  AddContestInput,
  AddContestResponse,
  Ballot,
  CreateBallotInput,
  CreateBallotResponse,
  DeliverBallotInput,
  DeliverBallotResponse,
} from './types';

@Resolver(() => Ballot)
export class BallotResolver {
  constructor(private readonly service: BallotService) {}

  @Query(() => Ballot)
  async ballot(@Args('id') id: ID) {
    return this.service.findById(id);
  }

  @Mutation(() => CreateBallotResponse)
  async createBallot(@Args('input') input: CreateBallotInput) {
    try {
      await this.service.createBallot(input.title);

      return {
        message: 'ballot created',
        ballot: {
          id: '1234',
          title: 'test title',
          status: 'Created',
        },
      };
    } catch (error) {
      console.log('error', error);

      return {
        message: 'ballot could not be created',
      };
    }
  }

  @Mutation(() => DeliverBallotResponse)
  async deliverBallot(@Args('input') input: DeliverBallotInput) {
    try {
      await this.service.deliverBallot(input.id, input.to);

      return {
        message: `ballot ${input.id} delivered`,
        ballot: {
          id: input.id,
          status: 'Delivered',
        },
      };
    } catch (error) {
      console.log('error', error);
      return {
        message: 'ballot could not be delivered',
      };
    }
  }

  @Mutation(() => AddContestResponse)
  async addContest(@Args('input') input: AddContestInput) {
    try {
      await this.service.addContest(input.ballotId, input.contest);

      return {
        message: `ballot ${input.ballotId} delivered`,
        ballot: {
          id: input.ballotId,
          status: 'Created',
        },
      };
    } catch (error) {
      console.log('error', error);
      return {
        message: 'contest could not be added to ballot',
      };
    }
  }
}
