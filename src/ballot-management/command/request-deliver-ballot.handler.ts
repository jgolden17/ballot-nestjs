import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { StoreEventPublisher, EventStore } from 'event-sourcing-nestjs';
import { EVENT_AGGREGATE } from '../constants';
import { Ballot } from '../domain/ballot.aggregate';
import { AbstractCommandHandler } from './abstract-command-handler';
import { RequestDeliverBallotCommand } from './request-deliver-ballot.command';

@CommandHandler(RequestDeliverBallotCommand)
export class RequestDeliverBallotHandler
  extends AbstractCommandHandler
  implements ICommandHandler<RequestDeliverBallotCommand>
{
  constructor(
    protected readonly publisher: StoreEventPublisher,
    protected readonly eventStore: EventStore,
  ) {
    super();
  }

  async execute(command: RequestDeliverBallotCommand) {
    const { ballotId, to } = command;

    const ballot = new Ballot(ballotId);

    ballot.loadFromHistory(
      await this.eventStore.getEvents(EVENT_AGGREGATE, ballotId),
    );

    ballot.deliver(to);

    this.publisher.mergeObjectContext(ballot).commit();
  }
}
