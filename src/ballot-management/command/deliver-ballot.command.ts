import { ICommand } from '@nestjs/cqrs';

export class DeliverBallotCommand implements ICommand {
  constructor(public readonly ballotId, public readonly to: Email[]) {}
}
