import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { StoreEventPublisher, EventStore } from 'event-sourcing-nestjs';
import { EVENT_AGGREGATE } from '../constants';
import { Ballot } from '../domain/ballot.aggregate';
import { AbstractCommandHandler } from './abstract-command-handler';
import { RemoveContestFromBallotCommand } from './remove-contest-from-ballot.command';

@CommandHandler(RemoveContestFromBallotCommand)
export class RemoveContestFromBallotHandler
  extends AbstractCommandHandler
  implements ICommandHandler<RemoveContestFromBallotCommand>
{
  constructor(
    protected readonly publisher: StoreEventPublisher,
    protected readonly eventStore: EventStore,
  ) {
    super();
  }

  async execute(command: RemoveContestFromBallotCommand) {
    const { ballotId, contestId } = command;

    const ballot = new Ballot(ballotId);

    ballot.loadFromHistory(
      await this.eventStore.getEvents(EVENT_AGGREGATE, ballotId),
    );

    ballot.removeContest(contestId);

    this.publisher.mergeObjectContext(ballot).commit();
  }
}
