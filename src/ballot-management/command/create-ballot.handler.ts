import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { StoreEventPublisher, EventStore } from 'event-sourcing-nestjs';
import { EVENT_AGGREGATE } from '../constants';
import { Ballot } from '../domain/ballot.aggregate';
import { AbstractCommandHandler } from './abstract-command-handler';
import { CreateBallotCommand } from './create-ballot.command';

@CommandHandler(CreateBallotCommand)
export class CreateBallotHandler
  extends AbstractCommandHandler
  implements ICommandHandler<CreateBallotCommand>
{
  constructor(
    protected readonly publisher: StoreEventPublisher,
    protected readonly eventStore: EventStore,
  ) {
    super();
  }

  async execute(command: CreateBallotCommand) {
    console.log('executing CreateBallot command');

    try {
      const { title } = command;

      const id = this.generateId();

      console.log('id', id);

      const ballot = new Ballot(id);

      ballot.loadFromHistory(
        await this.eventStore.getEvents(EVENT_AGGREGATE, id),
      );

      ballot.create(title);

      this.publisher.mergeObjectContext(ballot).commit();
    } catch (error) {
      console.error(error);
    }
  }
}
