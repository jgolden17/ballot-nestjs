import { ICommand } from '@nestjs/cqrs';

export class RemoveContestFromBallotCommand implements ICommand {
  constructor(public readonly ballotId: ID, public readonly contestId: ID) {}
}
