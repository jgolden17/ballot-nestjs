import { Types } from 'mongoose';

export abstract class AbstractCommandHandler {
  generateId() {
    return new Types.ObjectId().toString();
  }
}
