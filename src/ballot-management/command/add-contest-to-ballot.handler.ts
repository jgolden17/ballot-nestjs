import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { StoreEventPublisher, EventStore } from 'event-sourcing-nestjs';
import { isValidObjectId } from 'mongoose';
import { EVENT_AGGREGATE } from '../constants';
import { Ballot } from '../domain/ballot.aggregate';
import { Contest } from '../domain/contest';
import { AbstractCommandHandler } from './abstract-command-handler';
import { AddContestToBallotCommand } from './add-contest-to-ballot.command';

@CommandHandler(AddContestToBallotCommand)
export class AddContestToBallotHandler
  extends AbstractCommandHandler
  implements ICommandHandler<AddContestToBallotCommand>
{
  constructor(
    protected readonly publisher: StoreEventPublisher,
    protected readonly eventStore: EventStore,
  ) {
    super();
  }

  async execute(command: AddContestToBallotCommand) {
    console.log('executing AddContestToBallot command');

    try {
      const { ballotId } = command;

      const contest = Contest.create(this.generateId(), command.contest);

      const ballot = new Ballot(ballotId);

      ballot.loadFromHistory(
        await this.eventStore.getEvents(EVENT_AGGREGATE, ballotId),
      );

      ballot.addContest(contest);

      this.publisher.mergeObjectContext(ballot).commit();
    } catch (error) {
      console.error(error);
    }
  }
}
