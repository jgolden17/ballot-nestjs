import { ICommand } from '@nestjs/cqrs';

export class CreateBallotCommand implements ICommand {
  constructor(public readonly title: string) {}
}
