import { ICommand } from '@nestjs/cqrs';

export class RequestDeliverBallotCommand implements ICommand {
  constructor(public readonly ballotId, public readonly to: Email[]) {}
}
