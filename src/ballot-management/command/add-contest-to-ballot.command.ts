import { ICommand } from '@nestjs/cqrs';
import { IContest } from '../domain/contest';

export class AddContestToBallotCommand implements ICommand {
  constructor(
    public readonly ballotId: string,
    public readonly contest: Omit<IContest, 'id'>,
  ) {}
}
