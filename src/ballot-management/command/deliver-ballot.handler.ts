import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { StoreEventPublisher, EventStore } from 'event-sourcing-nestjs';
import { v4 as uuid } from 'uuid';
import { AbstractCommandHandler } from './abstract-command-handler';
import { DeliverBallotCommand } from './deliver-ballot.command';

@CommandHandler(DeliverBallotCommand)
export class DeliverBallotHandler
  extends AbstractCommandHandler
  implements ICommandHandler<DeliverBallotCommand>
{
  constructor(
    protected readonly publisher: StoreEventPublisher,
    protected readonly eventStore: EventStore,
  ) {
    super();
  }

  async execute(command: DeliverBallotCommand) {
    const { to } = command;

    to.forEach((voterEmail) => {
      const token = uuid();
      console.log('emailing ballot', token, 'to', voterEmail);
    });
  }
}
