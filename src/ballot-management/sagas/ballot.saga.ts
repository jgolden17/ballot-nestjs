import { Injectable } from '@nestjs/common';
import { ICommand, ofType, Saga } from '@nestjs/cqrs';
import { map, Observable } from 'rxjs';
import { DeliverBallotCommand } from '../command/deliver-ballot.command';
import { DeliverBallotRequested } from '../events/deliver-ballot-requested.event';

@Injectable()
export class BallotSagas {
  @Saga()
  deliverBallotRequested(events$: Observable<any>): Observable<ICommand> {
    return events$.pipe(
      ofType(DeliverBallotRequested),
      map((event) => new DeliverBallotCommand(event.id, event.data.to)),
    );
  }
}
