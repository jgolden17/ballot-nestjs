import { IQuery } from '@nestjs/cqrs';

export class GetBallotByIdQuery implements IQuery {
  constructor(public readonly id: ID) {}
}
