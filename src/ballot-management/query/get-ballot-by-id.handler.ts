import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { IBallot } from '../domain/ballot.aggregate';
import { BallotRepository } from '../projections/ballot.repository';
import { GetBallotByIdQuery } from './get-ballot-by-id.query';

@QueryHandler(GetBallotByIdQuery)
export class GetBallotByIdHandler
  implements IQueryHandler<GetBallotByIdQuery, IBallot>
{
  constructor(protected readonly repository: BallotRepository) {}

  async execute(query: GetBallotByIdQuery) {
    console.log('executing GetBallotByIdQuery');
    const { id } = query;

    const result = await this.repository.findById(id);

    console.log('result', result);

    return result;
  }
}
