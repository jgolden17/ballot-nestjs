export const BALLOT_CAST_CANNOT_RECAST =
  'ballot is already cast. cannot re-cast a casted ballot';

export const BALLOT_CAST_CANNOT_DELIVER =
  'ballot is already cast. cannot deliver a casted ballot';

export const BALLOT_COUNTED_CANNOT_RECOUNT =
  'ballot is already counted. cannot recount a ballot';
