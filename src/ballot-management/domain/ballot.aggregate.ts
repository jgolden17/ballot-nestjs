import { AggregateRoot } from '@nestjs/cqrs';
import { InvariantError } from 'src/core/InvariantError';
import { BallotCasted } from '../events/ballot-casted.event';
import { BallotCounted } from '../events/ballot-counted.event';
import { BallotCreated } from '../events/ballot-created.event';
import { BallotDelivered } from '../events/ballot-delivered.event';
import { ContestAddedToBallot } from '../events/contest-added-to-ballot.event';
import { ContestRemovedFromBallot } from '../events/contest-removed-from-ballot.event';
import { DeliverBallotRequested } from '../events/deliver-ballot-requested.event';
import {
  BALLOT_CAST_CANNOT_DELIVER,
  BALLOT_CAST_CANNOT_RECAST,
  BALLOT_COUNTED_CANNOT_RECOUNT,
} from './constants';
import { IContest } from './contest';

export enum BallotStatus {
  Cast = 'Cast',
  DeliveryPending = 'DeliveryPending',
  Delivered = 'Delivered',
  Ready = 'Ready',
  Created = 'Created',
  Counted = 'Counted',
}

export interface IBallot {
  id: ID;
  title: string;
  createdAt: Date;
  status: BallotStatus;
  contests: IContest[];
}

export class Ballot extends AggregateRoot implements IBallot {
  id: ID;

  title: string;

  createdAt: Date;

  status: BallotStatus;

  contests: IContest[];

  deliveredTo: Email[];

  constructor(id: ID) {
    super();

    this.id = id;
    this.contests = [];
  }

  create(title) {
    this.apply(
      new BallotCreated(this.id, {
        title,
        createdAt: new Date(),
      }),
    );
  }

  cast() {
    if (this.status === BallotStatus.Cast) {
      throw new InvariantError(BALLOT_CAST_CANNOT_RECAST);
    }

    this.apply(new BallotCasted(this.id, this));
  }

  count() {
    if (this.status !== BallotStatus.Counted) {
      throw new InvariantError(BALLOT_COUNTED_CANNOT_RECOUNT);
    }

    this.apply(
      new BallotCounted(this.id, {
        countedAt: new Date(),
      }),
    );
  }

  deliver(to) {
    if (to.length === 1) {
      throw new InvariantError('Must deliver ballot to at least one voter');
    }

    if (this.status === BallotStatus.Cast) {
      throw new InvariantError(BALLOT_CAST_CANNOT_DELIVER);
    }

    if (this.status === BallotStatus.Ready) {
      this.apply(
        new DeliverBallotRequested(this.id, {
          to,
          requestedAt: new Date(),
        }),
      );
    }
  }

  addContest(contest: IContest) {
    if (this.status !== BallotStatus.Created) {
      throw new InvariantError(
        `Cannot add contests to a ${this.status.toLowerCase()} ballot`,
      );
    }

    this.apply(new ContestAddedToBallot(this.id, contest));
  }

  removeContest(contestId: ID) {
    const contestToRemove = this.contests.find(
      (contest) => contest.id !== contestId,
    );

    if (contestToRemove) {
      this.apply(new ContestRemovedFromBallot(this.id, { id: contestId }));
    }
  }

  onBallotCreated(event: BallotCreated) {
    const { title, createdAt } = event.data;

    this.title = title;
    this.status = BallotStatus.Created;
    this.createdAt = createdAt;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onBallotCasted(event: BallotCasted) {
    this.status = BallotStatus.Cast;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onBallotDelivered(event: BallotDelivered) {
    this.status = BallotStatus.Delivered;
  }

  onContestAddedToBallot(event: ContestAddedToBallot) {
    this.contests.push(event.data);
  }

  onContestRemovedFromBallot(event: ContestRemovedFromBallot) {
    const { id: contestId } = event.data;

    this.contests = this.contests.filter((contest) => contest.id !== contestId);
  }

  onDeliverBallotRequested(event: DeliverBallotRequested) {
    const { to } = event.data;

    this.deliveredTo = to;
    this.status = BallotStatus.DeliveryPending;
  }
}
