import { InvariantError } from 'src/core/InvariantError';

export enum ContestType {
  Plurality = 'Plurality',
  RankedSimple = 'RankedSimple',
  RankedEnhanced = 'RankedEnhanced',
  Condorcet = 'Condorcet',
  Approval = 'Approval',
}

export interface IContest {
  id: ID;
  type: ContestType;
  title?: string;
  description?: string;
  choices: string[];
}

export class Contest implements IContest {
  id: ID;

  type: ContestType;

  title?: string;

  description?: string;

  choices: string[];

  private constructor(id: ID, props: IContest) {
    this.id = id;
    this.type = props.type;
    this.title = props.title;
    this.description = props.description;
    this.choices = props.choices;
  }

  static create(id: ID, props: Partial<IContest>): IContest {
    if (!props.type) {
      throw new InvariantError('contest type is required');
    }

    if (!props.choices || props.choices.length < 2) {
      throw new InvariantError('contest must have at least two choices');
    }

    return new Contest(id, props as IContest);
  }
}
