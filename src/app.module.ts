import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';
import { EventSourcingModule } from 'event-sourcing-nestjs';
import { TerminusModule } from '@nestjs/terminus';
import { HttpModule } from '@nestjs/axios';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BallotModule } from './ballot-management/ballot.module';
import { HealthController } from './health/health.controller';

@Module({
  imports: [
    TerminusModule,
    HttpModule,
    BallotModule,
    EventSourcingModule.forRoot({
      mongoURL: process.env.MONGO_CONNECTION_STRING,
    }),
    GraphQLModule.forRoot({
      autoSchemaFile: true,
    }),
    MongooseModule.forRoot(process.env.MONGO_CONNECTION_STRING),
  ],
  controllers: [AppController, HealthController],
  providers: [AppService],
})
export class AppModule {}
