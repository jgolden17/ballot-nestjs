declare type ID = string;
declare type Email = string;
declare type OmitId<T> = Omit<T, 'id'>;
