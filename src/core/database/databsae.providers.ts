import { Module } from '@nestjs/common';
import { providers } from './database.module';

@Module({
  providers: [...providers],
  exports: [...providers],
})
export class DatabaseModule {}
