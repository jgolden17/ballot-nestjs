import * as mongoose from 'mongoose';
import { DATABASE_CONNECTION } from '../constants';

export const providers = [
  {
    provide: DATABASE_CONNECTION,
    useFactory(): Promise<typeof mongoose> {
      return mongoose.connect(process.env.MONGO_CONNECTION_STRING);
    },
  },
];
