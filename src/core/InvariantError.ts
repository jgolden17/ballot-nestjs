export class InvariantError extends Error {
  constructor(public readonly message) {
    super();
  }
}
